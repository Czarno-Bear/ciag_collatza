'''
1. Start programu.
2. Zaimportowanie sys.
3. Jezeli ilosc elementow listy sys.argv jest rozna od 2, nastepuje blad,
    stosowny komunikat i zakonczenie programu.
4. Jezeli x jest mniejsze niz 1 lub wieksze niz 100, nastepuje blad, stosowny
    komunikat i zakonczenie programu.
5. Jezeli x spelnia warunki nastepuja stosowne obliczenia az x bedzie rowne 1.
6. Koniec programu.
'''

import sys

if len(sys.argv) != 2:
    print('Podano zla ilosc argumentow!')
else:

    x = float(sys.argv[1])
    print(x)

    if x < 1 or x > 100:
        print('Podano zla wartosc!')
    else:
        while x:
            if x % 2 == 0:
                x /= 2
                print(x)
            elif x == 1:
                print('Koniec')
                break
            else:
                x = (3 * x) + 1
                print(x)
